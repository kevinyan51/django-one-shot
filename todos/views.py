from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        'TodoList_object': list
    }
    return render(request, 'TodoLists/list.html', context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        'TodoListDetail_object': list
    }
    return render(request, 'TodoLists/detail.html', context)


def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoListForm()
    context = {
        'form': form
    }
    return render(request, 'TodoLists/create.html', context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        'form': form
    }
    return render(request, 'TodoLists/todo_list_update.html', context)


def todo_list_delete(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todo_instance.delete()
        return redirect('todo_list_list')
    return render(request, 'TodoLists/delete.html')


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form,
    }
    return render(request, 'TodoLists/item_create.html', context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        'form': form
    }
    return render(request, 'TodoLists/item_update.html', context)
